FROM node:10-alpine as build

WORKDIR /app

RUN apk add --no-cache --update ca-certificates python make bash

COPY package*.json ./

RUN npm install

COPY abin abin
COPY src src
COPY .eslintrc.yml mocha-reporters-config.json ./

CMD [ "abin/commit-test.sh" ]

# Remove development dependencies
FROM build as prerelease

RUN npm prune --production

# Release image
FROM node:10-alpine

LABEL org.opencontainers.image.authors="Shane Gibbons <shane.gibbons@gmail.com>"

RUN apk add --no-cache --update tini

WORKDIR /app

ENV NODE_ENV production
EXPOSE 3000

USER node

COPY --from=prerelease /app/package.json ./
COPY --from=prerelease /app/node_modules/ ./node_modules/
COPY --from=prerelease /app/src/app/ ./src/app/

# Tini allows node to accept signals (like SIGTERM)
ENTRYPOINT [ "/sbin/tini", "--" ]

CMD [ "node", "/app/src/app/server.js"]
