const express = require('express');
const bunyan = require('bunyan');
const data = require('./data');

const log = bunyan.createLogger({ name: 'hello-demo' });

const server = express();
const port = process.env.PORT || 3000;

server.get('/', (req, res) => {
  const message = 'Hello, Brian Kernighan!';
  log.info(message);
  res.json({ message });
});

server.get('/songs', (req, res) => {
  res.json(data);
});

server.get('/songs/:id', (req, res) => {
  const songId = parseInt(req.params.id, 10);
  const song = data.find((_song) => _song.id === songId);

  if (song) {
    log.info(`${song.lyric} (${song.artist}, ${song.releaseYear})`);
    res.json(song);
  } else {
    res.status(404).json({ message: `Song with id=${songId} not found.` });
  }
});

server.get('/health', (req, res) => {
  if (data) {
    res.status(200).end();
  }
  res.status(500).end();
});

server.listen(port, () => {
  log.info(`hello ReSTful API server started on port ${port}.`);
});
