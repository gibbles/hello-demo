#! /bin/sh

# Only ever used by the CI tool
# You should set up your gke and kubernetes authentication locally,
# and helm init locally as well.

# Code and process shamelessly lifted from https://medium.com/@yanick.witschi/automated-kubernetes-deployments-with-gitlab-helm-and-traefik-4e54bec47dcf

# The service account file (from gke iam service account) was base64 encoded
# and stuffed into the GKE_SERVICE_ACCOUNT variable in GitLab
# All GKE_* variables should be set up to be provided by the CI tool.

mkdir -p /etc/deploy
echo ${GKE_SERVICE_ACCOUNT} | base64 -d > /etc/deploy/sa.json
gcloud auth activate-service-account --key-file /etc/deploy/sa.json --project=${GKE_PROJECT}
gcloud container clusters get-credentials ${GKE_CLUSTER_NAME} --zone ${GKE_ZONE} --project ${GKE_PROJECT}

helm init --service-account tiller --wait --upgrade
